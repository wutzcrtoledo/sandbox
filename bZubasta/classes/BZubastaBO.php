<?php
include_once dirname(__FILE__).'/BZubastaDAO.php';
class BZubastaBO {
  private $daoObj;
  
  public function __construct() {
      $this->daoObj = new BZubastaDAO();      
  } 
 
  public function addZbEvent($event){
        return $this->daoObj->makeProductUnAvailable($event["id_product"]) && 
               $this->daoObj->addZbEvent($event);
  }

  public function updateAvailability($prodId){
        $res = $this->daoObj->getZbProdEvent($prodId);
        if($res != null && $res["is_finished"] == 0){
            $this->daoObj->makeProductUnAvailable($prodId);
        }
  }

  public function getZbEvents($id_product, $simplify = false){
        $res = $this->daoObj->getZbProdEvent($id_product);
        if($res == null)
            $res = array("notFound" => true);
        else{
            $res["id_product"] = $id_product;
            if($res["asigned_to_customer"] == null && $res["is_finished"] == 1){
                $this->closeEvent($id_product);
            }
           if($simplify)
                return $res;
              
            $curr =  Currency::getCurrencyInstance(Context::getContext()->currency->id);
            $res["currency"] = $curr;
            $res["init_price"] = ((float)$res["init_price"] * (float)$curr->conversion_rate);
            $res["formatted_init_price"] = $curr->sign ."". number_format($res["init_price"], $curr->decimals, '.', '') . " ".$curr->iso_code;
            $res["timezone"] = Configuration::get('PS_TIMEZONE');
        }         
        return $res;
  }
  
  public function addBid($bid){
    try{
        $custId = Context::getContext()->customer->id;
        if(!isset($custId))
            return array("OK" => false,"noUser" => true);
        $bid["customer_id"] = $custId;
        $bid["currency"] = Configuration::get('PS_CURRENCY_DEFAULT');
        $currEv = $this->getZbEvents($bid["id_product"], true);
        if((float)$bid["amount"] <= (float)$currEv["init_price"]){
            return array("OK" => false,"wrongAmount" => true);
        }
        $currEv["init_price"] = (float)$bid["amount"];
        $allOK = $this->daoObj->updateProducPrice($bid["id_product"], $bid["amount"]) && 
                 $this->daoObj->updateZbEvPrice($currEv) && 
                 $this->daoObj->addBid($bid);
        if($allOK)
             return array("OK" => true);

        return array("OK" => false,"noInsert" => true);        
    }
    catch(Exception $ex){
        return array("OK" => false,"internalErr" => true, "err" => $ex);    
    }
  }

  public function getProdBids($prod){
    try{
        $curr =  Currency::getCurrencyInstance(Context::getContext()->currency->id);
        $res = $this->daoObj->getZbProdBids($prod["id_product"]);
        $custId = Context::getContext()->customer->id;
        if($res == null)
            return array("notFound" => true);
        $res["amount"] = ((float)$res["amount"] * (float)$curr->conversion_rate);
        $out = array("firstname"  => $res["firstname"],
                     "lastname"  => $res["lastname"],
                     "amount"  => $res["amount"]);

        if($res["asigned_to_customer"] !== null){
            $out["finished"] = true;
         //   print_r($res["asigned_to_customer"]);
          //  print_r($custId);
            if($res["asigned_to_customer"] == $custId){
                
                $out["token"] = $res["unlock_token"];
            }
        }
        return array("OK" => true,"item"=>$out);        
    }
    catch(Exception $ex){
        return array("OK" => false,"internalErr" => true, "err" => $ex);    
    }
  }
  public function closeEvent($id_product){
    try{
        $event = $this->daoObj->getZbProdBids($id_product);
        $event["id_product"] = $id_product;
        $event["token"] = bin2hex(openssl_random_pseudo_bytes(64));
        //print_r($event);
        $this->daoObj->assignZbEventTo($event);
        return array("OK" => true);        
    }
    catch(Exception $ex){
        return array("OK" => false,"internalErr" => true, "err" => $ex);    
    }

  }
  public function unlockProduct($obj){
    try{//id_tax_rules_group getTaxRate()
        //$prod = new Product($obj["id_product"]);
      //  $taxRate = $this->daoObj->getTaxRate($prod->id_tax_rules_group);
        $event = $this->daoObj->getZbProdBids($obj["id_product"]);
        $custId = Context::getContext()->customer->id;
        $allOK = $event["asigned_to_customer"] == $custId && $event["unlock_token"] == $obj["token"];
        if($allOK){                              
            $this->daoObj->releaseProduct($obj["id_product"],$event["amount"]);
        }
        else
            return array("OK" => false, "wrongUserOrToken" => true);
        
        return array("OK" => true);        
    }
    catch(Exception $ex){
        return array("OK" => false,"internalErr" => true, "err" => $ex);    
    }
  }

  public function getProdIdsOnAuction(){
    try{
        $prodList = $this->daoObj->getZbProdsOnAuction();
        return array("OK" => true,"items"=> $prodList);        
    }
    catch(Exception $ex){
        return array("OK" => false,"internalErr" => true, "err" => $ex);    
    }

  }
}