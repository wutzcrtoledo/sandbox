<?php
class BZubastaDAO {
  public function __construct(){
  }

  public function addZbEvent($event){
    //id_product, start_date, end_date ,init_price
    $sql = "INSERT INTO "._DB_PREFIX_."bz_events(id_product, start_date, end_date, init_price) 
            VALUES(".$event["id_product"].", 
                   STR_TO_DATE('".$event["start_date"]."','%Y-%m-%d %h:%i %p'), 
                   STR_TO_DATE('".$event["end_date"]."','%Y-%m-%d %h:%i %p'), 
                   '".$event["init_price"]."'
                   ) 
            ON DUPLICATE KEY 
            UPDATE start_date = STR_TO_DATE('".$event["start_date"]."','%Y-%m-%d %h:%i %p'),
                   end_date = STR_TO_DATE('".$event["end_date"]."','%Y-%m-%d %h:%i %p'),
                   init_price = '".$event["init_price"]."',
                   asigned_to_customer = NULL, 
                   unlock_token = NULL";
    $res=Db::getInstance()->Execute($sql);
    return  $res;
  }

  public function makeProductUnAvailable($prod_id){
    $sql = "UPDATE "._DB_PREFIX_."product SET 
              available_for_order = 0,
              show_price = 1, 
              show_condition = 0 
            WHERE id_product = ".$prod_id;
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);
    $sql = "UPDATE "._DB_PREFIX_."product_shop SET 
                                    available_for_order = 0,
                                    show_price = 1, 
                                    show_condition = 0 
                                  WHERE id_product = ".$prod_id;
    $res=Db::getInstance()->Execute($sql);
    return ((int)$res > 0);
  }

  public function releaseProduct($prod_id, $finalPrice){
    $sql = "UPDATE "._DB_PREFIX_."product SET 
              available_for_order = 1,
              show_price = 1, 
              price = $finalPrice,
              show_condition = 0 
            WHERE id_product = ".$prod_id;
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);

    $sql = "UPDATE "._DB_PREFIX_."product_shop SET 
              available_for_order = 1,
              show_price = 1, 
              price = $finalPrice,
              show_condition = 0
            WHERE id_product = ".$prod_id;
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);
    return ((int)$res > 0);
  }

  public function updateProducPrice($prod_id, $finalPrice){
    $sql = "UPDATE "._DB_PREFIX_."product SET 
              price = $finalPrice
            WHERE id_product = ".$prod_id;
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);

    $sql = "UPDATE "._DB_PREFIX_."product_shop SET 
              price = $finalPrice
            WHERE id_product = ".$prod_id;
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);
    return ((int)$res > 0);
  }

  public function assignZbEventTo($event){
    $sql = "UPDATE "._DB_PREFIX_."bz_events SET 
            asigned_to_customer = '".$event["id_customer"]."' ,
            unlock_token = '".$event["token"]."'
            WHERE id_product = ".$event["id_product"];
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);
    return ((int)$res > 0);
  }

  public function updateZbEvPrice($event){
    $sql = "UPDATE "._DB_PREFIX_."bz_events SET 
            init_price = '".$event["init_price"]."' 
            WHERE id_product = ".$event["id_product"];
   // print_r($sql);
    $res=Db::getInstance()->Execute($sql);
    return ((int)$res > 0);
  }

  public function getZbProdEvent($prodId){
    $currTimez = Configuration::get('PS_TIMEZONE');
    $rightNow = new DateTime("now", new DateTimeZone($currTimez) );    
    $sql= "SELECT start_date,end_date,init_price,asigned_to_customer, STR_TO_DATE('".$rightNow->format('Y-m-d H:i:s')."','%Y-%m-%d %T') >= end_date as is_finished FROM `"._DB_PREFIX_."bz_events` WHERE id_product = $prodId";
    $res=Db::getInstance()->ExecuteS($sql);
    return  isset($res[0])?$res[0]:null;
  }

  public function getZbProdsOnAuction(){
    $currTimez = Configuration::get('PS_TIMEZONE');
    $rightNow = new DateTime("now", new DateTimeZone($currTimez) );    
    $sql = "SELECT id_product FROM "._DB_PREFIX_."bz_events WHERE STR_TO_DATE('".$rightNow->format('Y-m-d H:i:s')."','%Y-%m-%d %T') BETWEEN start_date and end_date";
    $res=Db::getInstance()->ExecuteS($sql);
    return  $res;
  }

  public function getZbProdBids($prodId){
    $sql = "SELECT bid.id_customer,
                   cus.firstname,
                   cus.lastname,
                   bid.amount,
                   ev.asigned_to_customer, 
                   ev.unlock_token
            FROM "._DB_PREFIX_."bz_events ev,
                 "._DB_PREFIX_."bz_bids bid, 
                 "._DB_PREFIX_."customer cus
            WHERE  ev.id_product = bid.id_product 
            AND bid.id_customer = cus.id_customer
            AND bid.id_product = $prodId 
            ORDER BY date DESC, amount ASC 
            LIMIT 1";

      $res=Db::getInstance()->ExecuteS($sql);
      return  isset($res[0])?$res[0]:null;
  }

  public function addBid($bid){
     $sql = "INSERT INTO "._DB_PREFIX_."bz_bids(id_customer,id_product,amount, currency) 
             VALUES (".$bid["customer_id"].",".$bid["id_product"].",".$bid["amount"].", ".$bid["currency"].")";
    // print_r($sql);
     $res=Db::getInstance()->Execute($sql);
     return ((int)$res > 0);
  }

  public function getTaxRate($taxRuleId){
    $sql = "SELECT tx.rate 
              FROM "._DB_PREFIX_."tax tx, 
                   "._DB_PREFIX_."tax_rule txr,
                   "._DB_PREFIX_."tax_rules_group txrg
              WHERE tx.id_tax = txr.id_tax 
              AND   txrg.id_tax_rules_group = txr.id_tax_rules_group
              AND   txrg.id_tax_rules_group = ".$taxRuleId;
    $res=Db::getInstance()->ExecuteS($sql);
    return  isset($res[0])?$res[0]["rate"]:null;
  }


}