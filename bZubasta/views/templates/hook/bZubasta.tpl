<section id="bZubasta_container" class="block" data-bind="if: bZubastaAvailable">
     <div data-bind="if: bzCurrItem().finished">  
           <h5 class="text-danger" > La subasta de este producto ha terminado </h5>
     </div>
     <!-- ko if: !bzCurrItem().finished -->
     <div>
        <span >{l s='The auction ends in' d='bZubasta'} ... </span>
     </div>
     <div>         
         <h5>
           <!-- ko if: countDown().days > 0 -->
           <span data-bind="html:countDown().days"></span><span> Dias&nbsp;</span>
           <!-- /ko -->
           <!-- ko if: countDown().hours > 0 -->
           <span data-bind="html:countDown().hours"></span><span> Horas&nbsp;</span>
           <!-- /ko -->
           <!-- ko if: countDown().minutes > 0 -->
           <span data-bind="html:countDown().minutes"></span><span> Minutos&nbsp;</span>
           <!-- /ko -->
           <span data-bind="html:countDown().seconds"></span><span> Segundos&nbsp;</span>
         </h5>
     </div>
     <!-- /ko -->
     <br/>
     <div>
          <h5>Precio en Subasta: <span data-bind="html: bidAmountFormatted() +' '+currency().iso_code"></span><small> (Sin Impuestos)</small></h5>
          <!-- ko if: customerAhead() && !bzCurrItem().finished -->
          <h6>En Delantera: <span class="text-warning" data-bind="html: customerAhead().firstName +' '+customerAhead().lastName"></span></h6>  
          <!-- /ko -->
          <!-- ko if: customerAhead() && bzCurrItem().finished -->
          <h6 class="text-success">Ganador: <span  data-bind="html: customerAhead().firstName +' '+customerAhead().lastName"></span></h6>  
          <!-- /ko -->
     </div>
     <br/>
     <!-- ko if: !bzCurrItem().finished -->
     <div>
        <span class="control-label">Aumentar a</span>      
        <div class="input-group m-b">
                  <span class="input-group-addon input-sm" data-bind="html:currency().iso_code + ' ' +currency().sign">-</span>
                  <input type="number" class="form-control input-sm" data-bind="value: bidAmount" type="number" name="bidAmount" id="bidAmount">
        </div>            
        <div class="bid-button-container">
            <button class="btn btn-primary" data-bind="click: raiseBid">
               <!-- ko if: !loadingBiding() -->
                <i class="material-icons">gavel</i>
               <!-- /ko -->
               <!-- ko if: loadingBiding() -->
                <i class="fa fa-spinner fa-spin"></i>
               <!-- /ko -->
              Bid
            </button>   
        </div>
    </div>
    <!-- /ko -->
    <!-- ko if: bzCurrItem().finished -->
        <div data-bind="if: unlockToken() && unlockToken() !== ''">
            <h6 class="text-success">¡Felicidades!, has ganado la subasta</h6>      
            <div class="unlock-button-container">
                <button class="btn btn-primary"  data-bind="click: unlockProduct">
                <!-- ko if: !loadingBiding() -->
                <i class="material-icons">lock_open</i>
                <!-- /ko -->
                <!-- ko if: loadingBiding() -->
                    <i class="fa fa-spinner fa-spin"></i>
                <!-- /ko -->
                Desbloquear Producto
                </button>   
            </div>
            <br/>
            <div class="alert alert-warning" role="alert"> 
                <small>
                    Al desbloquear el producto este quedará disponible para ser agregado al carro de compra. 
                    Si no está seguro de pasar por caja de inmediato. Recomendamos mantener bloqueado hasta que esté seguro de concretar la compra.
                    El producto quedará reservado durante 24 horas. <br/>Luego correrá la lista de espera al segundo comprador en cabeza.
                </small>
            </div>
        </div>
            <!-- ko if: unlockToken() === '' && customerAhead() -->
            <div class="alert alert-warning" role="alert"> 
                    <small>
                        El producto estará reservado para <span data-bind="html: customerAhead().firstName +' '+customerAhead().lastName">-</span> por 
                        24 horas. En caso de no concretar compra, correrá la lista de espera al segundo comprador en cabeza.
                    </small>
            </div>
            <!-- /ko -->
    <!-- /ko -->
    <div id="msgContainer" data-bind="if: displayMessage">
        {literal}
        <div data-bind="attr:{'class':'alert alert-'+outmessage().type},html:outmessage().message"  role="alert"> 
                    -
        </div>
        {/literal}
    </div>
  </section>
  <add-attributes style="visibility: hidden">
        <attr id="prop.success">{l s='Bid added OK!' d='bZubasta'}</attr>
        <attr id="prop.init_sess">{l s='You need to login to be able to participate on the auction' d='bZubasta'}</attr>
  </add-attributes>
  <script>
   var bZubastaInstance;
    var loadbZubasta = function(){        
        try {
            bZubastaInstance = new bZubasta();
            ko.applyBindings(bZubastaInstance, document.getElementById("bZubasta_container"));
            bZubastaInstance.loadProducto({$product|@json_encode nofilter});
        } catch (error) {
            console.log(error.message);
            setTimeout(function(){
                 console.log("Loading bZubasta");
                 loadbZubasta();
            },200);
        }            
    };
    loadbZubasta();

  </script>