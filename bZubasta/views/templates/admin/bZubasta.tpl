<!-- bZubastaAdmin -->
<div id="bZubastaAdmin">
        <input type='hidden' id="validation_passed" name="validation_passed" value="0"  />
        <div class="row">
                <div class="col-md-12">
                Configura subasta para el producto {$id_product}
                </div>
        </div>
        <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                                <label for="bz_start_date">Fecha de Inicio</label>
                                <div class="input-group datepicker">
                                                <input onblur="javascript: validateBZubastaForm();" type="text" class="form-control" id="bz_start_date" name="start_date" placeholder="YYYY-MM-DD" value="0000-00-00">
                                                <div class="input-group-append">
                                                        <div class="input-group-text"><i class="material-icons">date_range</i></div>
                                                </div>
                                </div>
                                <!-- input onblur="javascript: setEndateNextDay();" maxlength="10" size="10" type='text' id='bz_start_date' placeholder="DD/MM/YYYY" name="start_date" class="form-control" / -->
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                                <label for="bz_start_time">Hora Inicio</label>
                                <input onblur="javascript: validateBZubastaForm();" maxlength="8" size="8" type='text' id='bz_start_time' placeholder="HH:MM AM|PM" name="start_time" class="form-control" />                                       
                        </div>
                </div>
        </div>
        <div class="row">
                <div class="col-md-6">
                        <div class="form-group">
                                <label for="bz_end_date">Fecha de Termino</label>
                                <div class="input-group datepicker">
                                        <input onblur="javascript: validateBZubastaForm();" type="text" class="form-control" id="bz_end_date" name="end_date" placeholder="YYYY-MM-DD" value="0000-00-00">
                                        <div class="input-group-append">
                                                <div class="input-group-text"><i class="material-icons">date_range</i></div>
                                        </div>
                                </div>
                                <!-- input  maxlength="10" size="10" type='text' id='bz_end_date' placeholder="DD/MM/YYYY" name="end_date" class="form-control" / -->
                        </div>
                </div>
                <div class="col-md-6">
                        <div class="form-group">
                                <label for="bz_end_time">Hora Termino</label>
                                <input onblur="javascript: validateBZubastaForm();" maxlength="8" size="8" type='text' id='bz_end_time' placeholder="HH:MM AM|PM" name="end_time" class="form-control" />                                       
                        </div>
                </div>
        </div>        
        <div class="row">
                <div class="col-md-12">
                        <div class="form-group">
                                <label for="bz_init_price">Precio Inicial</label>
                                <div class="input-group">                        
                                        <div class="input-group-prepend">
                                        <span class="input-group-text">{$act_curr}$</span>
                                        </div>
                                        <input onblur="javascript: validateBZubastaForm();" name="init_price" id="bz_init_price" type="text" class="form-control" value="{$p_price}">       
                                </div>
                        </div>
                </div>
        </div>
        <div class="clear">&nbsp;</div>
</div>
<!-- /bZubastaAdmin -->