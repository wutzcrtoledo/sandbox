var bz_checkProdObjects = function(prodIdList){
    var gavelElem = ("<i class=\"material-icons\">gavel</i>");
    $(".product_item article, .product_item div.product-miniature").each(function(){
        console.log($(this).attr("data-id-product"));
        var prodId = $(this).attr("data-id-product");
       // prodId = parseInt(prodId);
        if(prodIdList.indexOf(prodId) !== -1){
            $(".product-title a", $(this)).prepend(gavelElem);
        }
    });
};

$(document).ready(function(){
    var auctionProdList = sessionStorage.getItem("bz_product_list_ids");
    if(!auctionProdList){ 
        ajaxAdmin.callService("ajxCtrl?action=getProdsOnAuction",null,"GET",function(_res){
            console.log(_res);
            if(_res.OK){
                auctionProdList = _.pluck(_res.items,"id_product");
                sessionStorage.setItem("bz_product_list_ids",JSON.stringify(auctionProdList));
                bz_checkProdObjects(auctionProdList);
            }
        });
    }
    else { 
        auctionProdList = JSON.parse(auctionProdList);
        bz_checkProdObjects(auctionProdList);
    }
});