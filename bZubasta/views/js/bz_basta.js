function bZubasta() {
    var mainMod = this;
    var defSelector = ".product-prices, "
    +".product-customization,"
    +".product-actions .product-variants, "
    +".product-actions .product-add-to-cart,"
    +".product-actions .product-additional-info .social-sharing,"
    +"#add-to-cart-or-refresh .product-add-to-cart,"
    +"#block-reassurance";

    var basicInitValidations = function(_prod){
        if(_prod && _prod.quantity && parseInt(_prod.quantity) <= 1){
                $(".product-quantity .qty input").attr("disabled",true);
        }
    };
    var showDefOptions = function(){
        $(defSelector).show();
        setTimeout(basicInitValidations, 200);
    };
    var hideDefOptions = function(){
        $(defSelector).hide();        
    };
    var getCountDown = function(endDate){
        console.log(endDate);
        endDate = new moment(endDate, "YYYY-MM-DD HH:mm:ss");
        var rightNow = new moment();
        var remainSeconds = endDate.unix() - rightNow.unix();
        console.log(remainSeconds);
        var resObg = {};
        //var remains = 0;
        resObg.days = Math.floor(remainSeconds/(60*60*24));
        remainSeconds = remainSeconds%(60*60*24);
        resObg.hours = Math.floor(remainSeconds/(60*60));
        remainSeconds = remainSeconds%(60*60);
        resObg.minutes = Math.floor(remainSeconds/60);
        remainSeconds = remainSeconds%60;
        resObg.seconds = Math.floor(remainSeconds);
        console.log(resObg);
        return resObg;
    };

    var startCountdown = function(){
        var countDown = mainMod.countDown();
        if(countDown.seconds === 0){
            countDown.seconds = 59;
            //countDown.minutes--;
            if(countDown.minutes === 0){
                countDown.minutes = 59;
                if(countDown.hours === 0){
                    countDown.hours = 23;
                    countDown.days--;
                }
                else
                    countDown.hours--;
            }
            else 
                countDown.minutes--;
        }
        else
            countDown.seconds--;
        mainMod.countDown(countDown);
        if(countDown.days <= 0 && countDown.hours <= 0 && countDown.minutes <= 0 && countDown.seconds <= 0){
            var itm = mainMod.bzCurrItem();
            itm.finished = true;
            mainMod.bzCurrItem(itm); 
        }
        if(!mainMod.bzCurrItem().finished)
            setTimeout(function(){startCountdown();}, 1000);
        else 
            refreshBids();
    };

    var setOutMessage = function(msg, type){
        var outMsg = mainMod.outmessage();
        outMsg.type = type||"success";
        outMsg.message = msg;
        mainMod.outmessage(outMsg);
        mainMod.displayMessage(true);
        setTimeout(function(){
            mainMod.displayMessage(false);
        },5000);        
    };

    var refreshBids = function(callback){
        var _parms = {id_product: mainMod.id_product()};
        ajaxAdmin.callService("ajxCtrl?action=getBids",_parms,"POST",function(_res){
            if(_res.OK){
                mainMod.bzCurrItem(_res.item);
                var formattedPrice = Math.formatCurrency(_res.item.amount, {symbol:mainMod.currency().sign,decimals:mainMod.currency().decimals});
                mainMod.bidAmount(Math.ceil(_res.item.amount*1.05));  
                mainMod.bidAmountFormatted(formattedPrice);
                mainMod.customerAhead({firstName:_res.item.firstname,lastName:_res.item.lastname});                  
                if(_res.item.finished){
                    if(_res.item.token)
                        mainMod.unlockToken(_res.item.token);
                }
            }
            if(callback)
                callback();
        });
    };

    var threadRefreshBids = function(){
        if(!mainMod.bzCurrItem().finished){
            refreshBids(function(){
                setTimeout(threadRefreshBids,6000);
            });
        }
    };

    var loadLocales = function(){
        var attrs = {};
        $("add-attributes attr").each(function(){
             var attName = $(this).attr("id");
             attrs[attName] = $(this).html();
        });
        mainMod.l(attrs);
    };

    //Public
    mainMod.id_product = ko.observable(null);
    mainMod.bZubastaAvailable = ko.observable(false);
    mainMod.loadingBiding = ko.observable(false);
    mainMod.bidAmount = ko.observable(1);
    mainMod.bidAmountFormatted = ko.observable(1);
    mainMod.currency = ko.observable(1);
    mainMod.countDown = ko.observable({days:-1,hours:-1,minutes:-1,seconds:-1});
    mainMod.outmessage = ko.observable({});
    mainMod.displayMessage = ko.observable(false);
    mainMod.customerAhead = ko.observable(null);
    mainMod.bzCurrItem = ko.observable({});
    mainMod.unlockToken = ko.observable("");
    mainMod.l = ko.observable({});

    mainMod.raiseBid = function(){
        console.log(mainMod.bidAmount());
        mainMod.loadingBiding(true);
        //Math.round(price/convrate)   
        var amount2Bid = Math.remCurrency(mainMod.bidAmount());
        amount2Bid = Math.round(amount2Bid/parseFloat(mainMod.currency().conversion_rate));
        var bidPar = {id_product: mainMod.id_product(), 
                      amount:amount2Bid};
        ajaxAdmin.callService("ajxCtrl?action=addBid",bidPar,"POST",function(_res){
            mainMod.loadingBiding(false);
            var formattedBid = Math.formatCurrency(mainMod.bidAmount(), {symbol:mainMod.currency().sign,decimals:mainMod.currency().decimals});
            if(_res.OK){
                setOutMessage(mainMod.l()["prop.success"]+" "+formattedBid);
                refreshBids();
            }
            else if(_res.noUser){
                setOutMessage(mainMod.l()["prop.init_sess"],"danger");
            }
            else if(_res.wrongAmount){
                setOutMessage("El monto a ingresar debe ser mayor al ya propuesto ","danger");
            }
            else{
                setOutMessage("Ha ocurrido un error, porfavor intente nuevamente más tarde ","danger");
            }
            console.log(_res);
        });    
    };
    mainMod.unlockProduct = function(){
        var bidPar = {id_product: mainMod.id_product(), 
                      token:mainMod.unlockToken()};
        ajaxAdmin.callService("ajxCtrl?action=releaseProduct",bidPar,"POST",function(_res){
            if(_res.OK){
                //showDefOptions();
                //mainMod.bZubastaAvailable(false);
                location.reload();
            }
            else{
                setOutMessage("Ha ocurrido un error, porfavor intente nuevamente más tarde ","danger");
            }
        });
    };
    mainMod.loadProducto = function(_prod){
        try {
            console.log(_prod);
            loadLocales();
            basicInitValidations(_prod);
            if(_prod.bz.notFound){
                        showDefOptions();
                        mainMod.bZubastaAvailable(false);
            }
            else{
                        hideDefOptions(); 
                        mainMod.id_product(_prod.id_product);
                        var formattedPrice = Math.formatCurrency(_prod.bz.init_price, {symbol:_prod.bz.currency.sign,decimals:_prod.bz.currency.decimals});
                        mainMod.countDown(getCountDown(_prod.bz.end_date));
                        mainMod.bidAmount(Math.ceil(_prod.bz.init_price*1.05));
                        mainMod.bidAmountFormatted(formattedPrice);
                        mainMod.currency(_prod.bz.currency);
                        mainMod.bZubastaAvailable(true);
                        threadRefreshBids();
                        $(".input-group.m-b .input-group-addon").css("font-size",10);
                        mainMod.bzCurrItem(_prod.bz);
                        startCountdown();
            }
        } catch (error) {
            console.log(error);
        }        
    };

    mainMod.serverDebug = function(){
        ajaxAdmin.callService("ajxCtrl?action=serverDebug",null,"POST",function(_res){
            console.log(_res);
        });
    };
}