(function ($) {
    $.each(['show', 'hide', 'display'], function (i, ev) {
      var el = $.fn[ev];
      $.fn[ev] = function () {
        this.trigger(ev);
        return el.apply(this, arguments);
      };
    });
  })(jQuery);

Math.remCurrency = function(total) {
    total = ""+total;
    return parseFloat(total.replace(/[^0-9\.]/ig,""));
};

Math.formatCurrency = function(total, opc) {
   var pref = opc?opc.symbol:"$";
   var dec = opc?(opc.decimals||2):2;
   pref = (pref === "none")?"":pref;
   if(typeof total === "string")
         total = Math.remCurrency(total);
   var neg = false;
   if(total < 0) {
       neg = true;
       total = Math.abs(total);
   }
   var toGo = (neg ? "-"+pref : pref) + parseFloat(total, 10).toFixed(dec).replace(/(\d)(?=(\d{3})+\.)/g, "$1,").toString();
   return toGo === pref+"NaN"?null:toGo;
};