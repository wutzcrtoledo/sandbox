
$(document).ready(function() {
    /** 
    $('#bz_start_date, #bz_end_date').datepicker({
        format: "dd/mm/yyyy",
        startDate: "today",
        orientation: "bottom auto",
        autoclose: true,
        todayHighlight: true
    });
    **/
    var startDate = moment();
    $("#bz_start_date").val(startDate.format("YYYY-MM-DD"));
    $("#bz_start_time").val(startDate.format("hh:mm a"));
    $("#bz_end_time").val(startDate.format("hh:mm a"));
    setEndateNextDay();
});

function setEndateNextDay(){
   var endDate = moment($("#bz_start_date").val(), "YYYY-MM-DD").add(1, 'days');
   $("#bz_end_date").val(endDate.format("YYYY-MM-DD"));
   validateBZubastaForm();
}

function validateBZubastaForm(){
      // var selector = "input.form-control";
      var dateFields = ["start_date","end_date"], timeFields = ["start_time","end_time"], numberFields = ["init_price"];
      var allOK = true;
      $("#bZubastaAdmin input.form-control").each(function(i){
            var curObj = $(this);
            var valid = false;
            if(dateFields.indexOf(curObj.attr("name")) !== -1)
                valid = /^\d{4}[\/\-](0?[1-9]|1[012])[\/\-](0?[1-9]|[12][0-9]|3[01])$/ig.test(curObj.val());
            else if(timeFields.indexOf(curObj.attr("name")) !== -1)
                 valid = /^([1-9]|0[1-9]|1[0-2]):[0-5][0-9]\sAM|PM$/ig.test(curObj.val());
            else if(numberFields.indexOf(curObj.attr("name")) !== -1)
                    valid = /[0-9\.\,]/ig.test(curObj.val());
            if(valid)
                curObj.css("border","1px solid green");
            else
                curObj.css("border","1px solid red");
            allOK = allOK && valid;
      }); 
      if(allOK)
          $("#validation_passed").val("1"); 
}