<?php
include_once dirname(__FILE__).'/../../classes/BZubastaBO.php';
class BZubastaAjxCtrlModuleFrontController extends ModuleFrontController{
	public function initContent(){
		$this->ajax = true;
		parent::initContent();		
	}

	public function displayAjaxTestCall(){
		header('Content-Type: application/json');

		$this->ajaxDie(Tools::jsonEncode([
			'askbid_popup' => "TEST RES",
			'product' => "TEST PROD"
		 ]));
	}

	public function displayAjaxGetBZProduct(){
		$bzubBo = new BZubastaBO();
		$body = file_get_contents('php://input');
		$body = json_decode($body, true);
		$_res = $bzubBo->getZbEvents($body["id_product"]);
		$_res["formattedPrice"] = Tools::displayPrice($_res['init_price'], Context::getContext()->currency);
		header('Content-Type: application/json');
		$this->ajaxDie(Tools::jsonEncode($_res));
	}

	public function displayAjaxAddBid(){
		$bzubBo = new BZubastaBO();
		$body = file_get_contents('php://input');
		$body = json_decode($body, true);
		$_res = $bzubBo->addBid($body);
		header('Content-Type: application/json');
		$this->ajaxDie(Tools::jsonEncode($_res));
	}

	public function displayAjaxGetBids(){
		$bzubBo = new BZubastaBO();
		$body = file_get_contents('php://input');
		$body = json_decode($body, true);
		$_res = $bzubBo->getProdBids($body);
		header('Content-Type: application/json');
		$this->ajaxDie(Tools::jsonEncode($_res));
	}

	public function displayAjaxReleaseProduct(){
		$bzubBo = new BZubastaBO();
		$body = file_get_contents('php://input');
		$body = json_decode($body, true);
		$_res = $bzubBo->unlockProduct($body);
		header('Content-Type: application/json');
		$this->ajaxDie(Tools::jsonEncode($_res));
	}

	public function displayAjaxGetProdsOnAuction(){
		$bzubBo = new BZubastaBO();
		$_res = $bzubBo->getProdIdsOnAuction();
		header('Content-Type: application/json');
		$this->ajaxDie(Tools::jsonEncode($_res));
	}
	

	public function displayAjaxServerDebug(){
		header('Content-Type: application/json');
		$obj = array("timezone" => Configuration::get('PS_TIMEZONE'),
					 "customer" => $this->context->customer,
					 "context" => Context::getContext(),
					 "logged" => $this->context->customer->isLogged(),
					 "langDefault" => Configuration::get('PS_LANG_DEFAULT'),
					 "tax" => Configuration::get('PS_TAX_RULE'),
					 "currencyDefault" => Currency::getCurrencyInstance((int)(Configuration::get('PS_CURRENCY_DEFAULT'))),
					 "productExample" => new Product(14));

		$this->ajaxDie(Tools::jsonEncode($obj));
	}

	
}