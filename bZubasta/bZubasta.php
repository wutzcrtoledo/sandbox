<?php
if (!defined('_PS_VERSION_'))
{
  exit;
}

//#91888B Azul Zinc
class BZubasta extends Module{
  public function __construct()  {
    $this->name = 'bZubasta';
    $this->tab = 'front_office_features';
    $this->version = '1.0.0';
    $this->author = 'Cristian Toledo';
    $this->need_instance = 0;
    $this->ps_versions_compliancy = array('min' => '1.6', 'max' => _PS_VERSION_);
    $this->bootstrap = true;

    parent::__construct();

    $this->displayName = $this->l('BZubasta - (Auction)');
    $this->description = $this->l('Make Auction on Prestashop products.');

    $this->confirmUninstall = $this->l('Are you sure you want to uninstall?');

    if (!Configuration::get('bZubasta'))
      $this->warning = $this->l('No name provided');
  }

  public function install(){
      $allOK = true;
      //SQL Installing "._DB_PREFIX_." product
      $sql = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."bz_events (
                                    id_product INT NOT NULL,
                                    start_date DATETIME DEFAULT NULL,
                                    end_date DATETIME DEFAULT NULL,
                                    init_price FLOAT DEFAULT 0,   
                                    asigned_to_customer INT, 
                                    unlock_token VARCHAR(100),                 
                                    PRIMARY KEY (id_product)
                                    )";
     
     if(!$result=Db::getInstance()->Execute($sql))
         return false;
      
        $sql = "CREATE TABLE IF NOT EXISTS "._DB_PREFIX_."bz_bids (
                        id INT NOT NULL AUTO_INCREMENT,
                        id_customer INT NOT NULL,
                        id_product INT NOT NULL,
                        amount FLOAT NOT NULL,
                        currency VARCHAR(10) NOT NULL,
                        date DATETIME NOT NULL DEFAULT CURRENT_TIMESTAMP,
                        PRIMARY KEY (id, id_customer,id_product),
                        INDEX fk_ps_bz_bids_ps_bz_id_productx (id_product ASC)) ";    
        if(!$result=Db::getInstance()->Execute($sql))
                return false;
    
    $allOK = parent::install();    
    //$allOK = $allOK && $this->setAttributes();
    $allOK = $allOK && $this->registerHook('displayProductAdditionalInfo');
    $allOK = $allOK && $this->registerHook('displayHeader');
    $allOK = $allOK && $this->registerHook('actionFooter');
    $allOK = $allOK && $this->registerHook('displayBackOfficeHeader');
    $allOK = $allOK && $this->registerHook('displayAdminProductsExtra');
    $allOK = $allOK && $this->registerHook('actionProductUpdate');
    $allOK = $allOK && Configuration::updateValue('bZubasta', 'my friend');    
    return $allOK;
  }
  /** 
  public function setAttributes(){
      $this->l('The auction ends in');
      $this->l('Bid added OK!');
      return true;
  }
  **/

  public function uninstall()    {
    $allOK = true;
    $sql= "DROP TABLE  "._DB_PREFIX_."bz_bids ";
    if(!$result=Db::getInstance()->Execute($sql))
        return false;

    $sql= "DROP TABLE  "._DB_PREFIX_."bz_events ";   
    if(!$result=Db::getInstance()->Execute($sql))
        return  false;
    if (!parent::uninstall())
        return  false;

    return $allOK;
  }
 
  public function getContent()    {
        $output = null;    
        if (Tools::isSubmit('submit'.$this->name)){
            $my_module_name = strval(Tools::getValue('bZubasta'));
            if (!$my_module_name
            || empty($my_module_name)
            || !Validate::isGenericName($my_module_name))
                $output .= $this->displayError($this->l('Invalid Configuration value'));
            else{
                Configuration::updateValue('bZubasta', $my_module_name);
                $output .= $this->displayConfirmation($this->l('Settings updated'));
            }
        }
        return $output.$this->displayForm();
  }
  public function displayForm()    {
        // Get default language
        $default_lang = (int)Configuration::get('PS_LANG_DEFAULT');
    
        // Init Fields form array
        $fields_form[0]['form'] = array(
            'legend' => array(
                'title' => $this->l('Settings'),
            ),
            'input' => array(
                array(
                    'type' => 'text',
                    'label' => $this->l('Configuration value'),
                    'name' => 'bZubasta',
                    'size' => 20,
                    'required' => true
                )
            ),
            'submit' => array(
                'title' => $this->l('Save'),
                'class' => 'btn btn-default pull-right'
            )
        );
    
        $helper = new HelperForm();
    
        // Module, token and currentIndex
        $helper->module = $this;
        $helper->name_controller = $this->name;
        $helper->token = Tools::getAdminTokenLite('AdminModules');
        $helper->currentIndex = AdminController::$currentIndex.'&configure='.$this->name;
    
        // Language
        $helper->default_form_language = $default_lang;
        $helper->allow_employee_form_lang = $default_lang;
    
        // Title and toolbar
        $helper->title = $this->displayName;
        $helper->show_toolbar = true;        // false -> remove toolbar
        $helper->toolbar_scroll = true;      // yes - > Toolbar is always visible on the top of the screen.
        $helper->submit_action = 'submit'.$this->name;
        $helper->toolbar_btn = array(
            'save' =>
            array(
                'desc' => $this->l('Save'),
                'href' => AdminController::$currentIndex.'&configure='.$this->name.'&save'.$this->name.
                '&token='.Tools::getAdminTokenLite('AdminModules'),
            ),
            'back' => array(
                'href' => AdminController::$currentIndex.'&token='.Tools::getAdminTokenLite('AdminModules'),
                'desc' => $this->l('Back to list')
            )
        );
    
        // Load current value
        $helper->fields_value['bZubasta'] = Configuration::get('bZubasta');
    
        return $helper->generateForm($fields_form);
    }

    public function hookDisplayProductAdditionalInfo($params) {
        include_once dirname(__FILE__).'/classes/BZubastaBO.php';
        $product = $params["product"];
        $bzubBo = new BZubastaBO();
        $_bzres = $bzubBo->getZbEvents($product["id_product"]);
        //$_bzres["formattedPrice"] = Tools::displayPrice($_bzres['init_price'], Context::getContext()->currency);
        //print_r($_bzres);
        $prodReleased = ((int)$product["available_for_order"]) > 0;
         
        if((!isset($_bzres["notFound"]) || $_bzres["notFound"] == null) && $prodReleased){
            //$event = $this->daoObj->getZbProdBids($obj["id_product"]);
            $custId = Context::getContext()->customer->id;
            $allOK = $_bzres["is_finished"] == 1 && $_bzres["asigned_to_customer"] == $custId;
            $product["bz"] = $allOK?array("notFound" => true):$_bzres;            
        }
        else {       
            $product["bz"] = $_bzres;
            $product["flags"] = array($product["condition"]["type"] => $product["condition"]); 
        }

         $tplPars = array('product' => $product);
         $this->context->smarty->assign($tplPars);
         return $this->display(__FILE__,"bZubasta.tpl");
    }

    public function hookDisplayHeader() {
       // print 'CTH: Pase por el modulo';
        $this->context->controller->addCSS($this->_path.'views/css/bz_basta.css');        
        $this->context->controller->addJS($this->_path.'views/js/lib/cust-plugin.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/AjaxAdmin.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/knockout-3.4.2.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/underscore.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/moment.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/moment-timezone.min.js');        
        $this->context->controller->addJS($this->_path.'views/js/bz_basta.js');
        $this->context->controller->addJS($this->_path.'views/js/bz_checkauctionProds.js');
    }

    public function hookActionFooter($params){
        // Create a link with the good path
        $link = new Link;        
        $parameters = array("action" => "testCall");
        $ajax_link = $link->getModuleLink('BZubasta','controller', $parameters);

       // print_r($ajax_link);
        debugger_print($ajax_link);
        Media::addJsDef(array(
            "ajax_link" => $ajax_link
        ));
    }    

    // -- CUSTOM TAB Preparing data for the form
    public function hookDisplayBackOfficeHeader(){
        $this->context->controller->addJS($this->_path.'views/js/lib/moment.js');
        $this->context->controller->addJS($this->_path.'views/js/lib/moment-timezone.min.js');        
        $this->context->controller->addJS($this->_path.'views/js/admin/bz_basta_admin.js');
    }
    public function hookDisplayAdminProductsExtra($params) {
            $id_product = $params['id_product'];
            $product = new Product($id_product);
            $curr = Currency::getCurrencyInstance((int)(Configuration::get('PS_CURRENCY_DEFAULT')));
            $this->context->smarty->assign(array(
                    'id_product' => $id_product,
                    'url_token' => Tools::getValue('_token'),
                    'p_price' => $product->price,
                    'act_curr' => $curr->iso_code
            ));
            return $this->display(__FILE__, 'views/templates/admin/bZubasta.tpl');
    } 
    //Saving Data
    public function hookActionProductUpdate($params) {
                include_once dirname(__FILE__).'/classes/BZubastaBO.php';
                $bzubBo = new BZubastaBO();
                $event = array();
                if(null !== Tools::getValue('start_date') && Tools::getValue('start_date') != "" && "0000-00-00 00:00:00" !== Tools::getValue('start_date')){
                   // print_r(Tools::getValue('validation_passed'));  
                    if((int)Tools::getValue('validation_passed') == 0)
                        return false;
                    $startDate = Tools::getValue('start_date') .' '.Tools::getValue('start_time');
                    $endDate = Tools::getValue('end_date') .' '.Tools::getValue('end_time');
                    $event["id_product"] = $params['id_product'];
                    $event["start_date"]= $startDate;
                    $event["end_date"] = $endDate;                    
                    $event["init_price"] = Tools::getValue('init_price');    
                    $bzubBo->addZbEvent($event);
                }
                $bzubBo->updateAvailability($params['id_product']);
           
            return  true;
    }
} // End Class