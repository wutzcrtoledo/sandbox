import React from 'react';
import {
  Button,
  Grid,
  Form,
  Segment,
  Icon
} from 'semantic-ui-react';


const Contacto = (props) => {    
    return(<Segment style={{ padding: '8em 0em' }} vertical inverted className="contacto-container">
                <Grid container stackable verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column width={16}>
                        <Form inverted>
                            <Form.Group widths='equal'>
                                <Form.Input fluid label='Nombre' placeholder='Nombre'  />
                                <Form.Input fluid label='Mail' placeholder='Mail' />                                
                            </Form.Group>
                            <Form.TextArea label='Mensaje' placeholder='Que necesita saber ? ...' />
                        </Form>
                    </Grid.Column>                    
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='center'>
                    <Button size='big'>Enviar &nbsp;&nbsp;
                          <Icon name='send' />
                    </Button>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='center' width={8}>
                        <Icon name='whatsapp' />&nbsp;+569-979-92453
                    </Grid.Column>
                    <Grid.Column textAlign='center' width={8}>
                        <Icon name='mail' />&nbsp;beserkerac@gmail.com
                    </Grid.Column>
                </Grid.Row>
                </Grid>                
            </Segment>);
};

export default Contacto;