import React from 'react';
import {
  Button,
  Grid,
  Icon,
  Image,
  Segment,
  Card,
  Divider,
  Responsive
} from 'semantic-ui-react';

const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

const isMobile = () => {
    //console.log(getWidth(), Responsive.onlyMobile.maxWidth, getWidth() < Responsive.onlyMobile.maxWidth);
    return (getWidth() < Responsive.onlyMobile.maxWidth);
};

const Servicios = (props) => {
    let {parentWindow} = props;
    const css = {
        cardCont : {width: !isMobile()?"47%":"98%"},
        cardDesc : {
            marginTop: 5
        },
        mainSegment : {
            padding: !isMobile()?'6em 0em':'3em 0em'
        }
    };
    return(<Segment style={css.mainSegment} vertical className="servicios-container">
                <Grid container stackable verticalAlign='middle'>
                <Grid.Row>
                    <Grid.Column width={16}>
                    <p style={{ fontSize: '1.33em' }}>
                        Prestamos servicios de contabilidad integrales a clientes con operaciones comerciales en Chile. En términos generales y dependiendo del tipo y tamaño de nuestros clientes prestamos los siguientes servicios:
                    </p>
                    <Card.Group>
                            <Card style={css.cardCont}>
                                <Card.Content>
                                    <Card.Header>CONTABLES</Card.Header>
                                    <Card.Description style={css.cardDesc}>
                                        <Image floated='left' size={isMobile()?'medium':'small'} rounded src='/img/finanzaServ2.jpg' />
                                        <Divider  />
                                        Preparacion de Formularios 29 <br/>
                                        Libro electronico de ventas <br/>
                                        Libro electronico de compras <br/>
                                        Propuesta mensula de pre-balance <br/>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                            <Card style={css.cardCont}>
                                <Card.Content>
                                    <Card.Header>LABORALES</Card.Header>
                                    <Card.Description style={css.cardDesc}>
                                        <Image floated='left' size={isMobile()?'medium':'small'} rounded src='/img/servFin3.jpeg' />    
                                        <Divider  />
                                        Confeccion de contratos de trabajo <br/>
                                        Confeccion de liquidaciones de sueldo <br/>
                                        Declacion de cotizaciones previsionales <br/>
                                        Confeccion de finiquitos <br/>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                            <Card style={css.cardCont}>
                                <Card.Content>
                                    <Card.Header>SERVICIOS ADICIONALES</Card.Header>
                                    <Card.Description style={css.cardDesc}>
                                        <Image floated='left' size={isMobile()?'medium':'small'} rounded src='/img/siilinea.jpg' />    
                                        <Divider  />
                                        Declaracion jurada de renta <br/>
                                        Declaracion anual de renta, F22 <br/>
                                        Apoyo en requerimientos de fiscalizacion por parte del SII <br/>
                                    </Card.Description>
                                </Card.Content>
                            </Card>
                            <Card style={css.cardCont}>
                                <Card.Content>
                                    <Card.Header>APOYO EMPRENDEDOR</Card.Header>
                                    <Card.Description style={css.cardDesc}>
                                        <Image floated='left' size={isMobile()?'medium':'small'} rounded src='/img/emprendedor.png' />    
                                        <Divider  />
                                        Constitucion de sociedades<br/>
                                        Direccion tributaria y comercial<br/>
                                        Inicio de actividades<br/>
                                        Gestion de patentes municipales<br/>
                                        Representacion legal en Chile<br/>
                                    </Card.Description>
                                </Card.Content>
                            </Card>                        
                    </Card.Group>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column textAlign='center'>
                    <Button size='big' onClick={()=>{parentWindow().loadPage("Contacto");}}>
                          Contáctenos&nbsp;&nbsp;
                          <Icon name='mail' />
                    </Button>
                    </Grid.Column>
                </Grid.Row>                
                </Grid>
            </Segment>);
};

export default Servicios;