import React from 'react';
import {
    Transition
} from 'semantic-ui-react';

import {Contacto,Servicios} from './pages';

const Pages = {"Contacto":Contacto, 
               "Servicios":Servicios};

class PageManager extends React.Component{
    constructor(props) {
        super(props);
        this.state = {
            Mod:()=>{return(<div></div>);}
        };
        this.pageLoaded="";
    }

    scrollToPage = () => {
        let y = document.getElementById("currPage").getBoundingClientRect().top + window.scrollY;
        window.scroll({
            top: y,
            behavior: 'smooth'
        });
    };

    scrollToTop = () => {
     //   let y = document.getElementById("currPage").getBoundingClientRect().top + window.scrollY;
        window.scroll({
            top: 0,
            behavior: 'smooth'
        });
    };

    loadNewModule = () => {
        //if(!this.state.processing){
            this.scrollToTop();
            let Mod = Pages[this.props.page]?Pages[this.props.page]:()=>{return(<div></div>)};
            this.setState({Mod:Mod,showPage:true});
            this.props.pageLoaded(this.props.page);
        //}
    }


    componentDidUpdate(pProps){
        console.log(pProps.page, this.props.page ,this.pageLoaded);
        if(this.props.page !== this.state.pageLoaded){          
            this.pageLoaded = this.props.page;
        }
    }

    render() {
        let { showPage, Mod } = this.state;
        showPage = this.props.page === this.pageLoaded;

        return(<Transition visible={showPage} animation={"fly up"} onHide={this.loadNewModule} >
                  <div id="currPage">
                      {<Mod parentWindow={this.props.parentWindow} />}
                  </div>
              </Transition>);

    }
}

export default PageManager;