import PropTypes from 'prop-types';
import React, { Component } from 'react';
import {
  Button,
  Container,
  Transition,
  Grid,
  Header,
  Icon,
  Image,
    Menu,
  Responsive,
  Segment,
  Sidebar,
  Visibility,
} from 'semantic-ui-react';

import PageManager from './PageManager';
import Footer from './global/Footer';

// Heads up!
// We using React Static to prerender our docs with server side rendering, this is a quite simple solution.
// For more advanced usage please check Responsive docs under the "Usage" section.
const getWidth = () => {
  const isSSR = typeof window === 'undefined';
  return isSSR ? Responsive.onlyTablet.minWidth : window.innerWidth;
};

/* eslint-disable react/no-multi-comp */
/* Heads up! HomepageHeading uses inline styling, however it's not the best practice. Use CSS or styled components for
 * such things.
 */
const HomepageHeading = ({ mobile, isHome, loadPage }) => (
  <Transition visible={isHome} animation={"fly up"} >
          <Grid container doubling stackable>
                <Grid.Row columns={1}>
                    <Grid.Column>
                    <Header
                          as='h1'
                          inverted
                          style={{
                            fontSize: mobile ? '2em' : '4em',
                            fontWeight: 'normal',
                            marginBottom: 50,
                            marginTop: mobile ? '0.5em' : '1.5em',
                          }}            
                        >
                        Berserker-AC
                        </Header>
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row columns={2} divided >
                    <Grid.Column>
                        <Header as='h2' inverted   >
                              Nuestra Idea
                          </Header>
                          <p>
                            Guiar a nuestros clientes desde el comienzo en todos los procesos del negocio con planes y servicios ajustados a sus necesidades y requerimientos, haciéndonos cargo de toda la administración comercial y contable.
                          </p>
                    </Grid.Column>
                    <Grid.Column>
                             <Header as='h2' inverted >
                                Nuestro compromiso
                             </Header>
                            <p>
                            La Contabilidad es una obligación que todo contribuyente debe realizar mensualmente, la cual consiste en administrar y registrar información fidedigna asociada a los ingresos y egresos de la persona o empresa, con el fin de cumplir con los requisitos tributarios y laborales.
                            </p> 
                    </Grid.Column>
                </Grid.Row>
                <Grid.Row>
                    <Grid.Column columns={1}>
                    <Button primary size='big' style={{marginTop:50}} onClick={(e)=>{loadPage('Servicios');}} >
                          Nuestros Servicios &nbsp;&nbsp;
                          <Icon  name='down arrow' />
                        </Button>
                    </Grid.Column>
                </Grid.Row>
          </Grid>
  </Transition>  
)

HomepageHeading.propTypes = {
  mobile: PropTypes.bool,
}

/* Heads up!
 * Neither Semantic UI nor Semantic UI React offer a responsive navbar, however, it can be implemented easily.
 * It can be more complicated, but you can create really flexible markup.
 */
class DesktopContainer extends Component {
  state = {}

  hideFixedMenu = () => this.setState({ fixed: false })
  showFixedMenu = () => this.setState({ fixed: true })

  render() {
    const { children, loadPage, activePage } = this.props
    const { fixed } = this.state

    let segmentStyle = {
        home:{ minHeight: 700, 
               padding: '1em 0em' 
              },
        notHome:{ height: 100, 
                  padding: '1em 0em' 
                }
    };

    return (
      <Responsive getWidth={getWidth} minWidth={Responsive.onlyTablet.minWidth}>
        <Visibility
          once={false}
          onBottomPassed={this.showFixedMenu}
          onBottomPassedReverse={this.hideFixedMenu}
        >
          <Segment 
            className="main-home-container"
            inverted
            textAlign='center'
            style={activePage==="Home"?segmentStyle.home:segmentStyle.notHome}
            vertical
          >
            <Menu
              fixed={fixed ? 'top' : null}
              inverted={!fixed}
              pointing={!fixed}
              secondary={!fixed}
              size='large'
            >
              <Container>
                <Menu.Item as='a' active={activePage==="Home"} onClick={()=>{loadPage('Home');}}>
                    <Image floated='left' size='mini' rounded src='/img/logo.png' />
                    B-AC
                </Menu.Item>
                <Menu.Item as='a' active={activePage==="Servicios"} onClick={()=>{loadPage('Servicios');}}>Servicios</Menu.Item>
                <Menu.Item as='a' active={activePage==="Contacto"} onClick={()=>{loadPage('Contacto');}}>Contacto</Menu.Item>                
              </Container>
            </Menu>
            <HomepageHeading isHome={activePage==="Home"} loadPage={loadPage} />
          </Segment>
        </Visibility>

        {children}
      </Responsive>
    )
  }
}

DesktopContainer.propTypes = {
  children: PropTypes.node,
}

class MobileContainer extends Component {
  state = {}

  handleSidebarHide = () => this.setState({ sidebarOpened: false })

  handleToggle = () => this.setState({ sidebarOpened: true })

  
  render() {
    const { children,loadPage,activePage} = this.props
    const { sidebarOpened } = this.state

    let segmentStyle = {
        home:{ minHeight: 350, 
               padding: '1em 0em' 
              },
        notHome:{ height: 80, 
                  padding: '1em 0em' 
                }
    };

    return (
      <Responsive
        as={Sidebar.Pushable}
        getWidth={getWidth}
        maxWidth={Responsive.onlyMobile.maxWidth}
      >
          <Sidebar
            as={Menu}
            animation='push'
            inverted
            onHide={this.handleSidebarHide}
            vertical
            visible={sidebarOpened}
          >
          <Menu.Item as='div'>
              <Image floated='left' size='mini' rounded src='/img/logo.png' />
              B-AC
          </Menu.Item>        
          <Menu.Item as='a' active={activePage==="Home"} onClick={()=>{loadPage('Home');this.handleSidebarHide();}}>
                Home
          </Menu.Item>
          <Menu.Item as='a' active={activePage==="Servicios"} onClick={()=>{loadPage('Servicios');this.handleSidebarHide();}}>Servicios</Menu.Item>
          <Menu.Item as='a' active={activePage==="Contacto"} onClick={()=>{loadPage('Contacto');this.handleSidebarHide();}}>Contacto</Menu.Item>    
        </Sidebar>

        <Sidebar.Pusher dimmed={sidebarOpened}>
          <Segment 
            className="main-home-container"
            inverted
            textAlign='center'
            style={activePage==="Home"?segmentStyle.home:segmentStyle.notHome}
            vertical
          >
            <Container>
              <Menu inverted pointing secondary size='large'>
                <Menu.Item onClick={this.handleToggle}>
                  <Icon name='sidebar' />&nbsp;&nbsp;
                  <Image floated='left' size='mini' rounded src='/img/logo.png' />&nbsp;
                  Berserker Asesoria Contable
                </Menu.Item>
              </Menu>
            </Container>
            <HomepageHeading isHome={activePage==="Home"} loadPage={loadPage} mobile />
          </Segment>

          {children}
        </Sidebar.Pusher>
      </Responsive>
    )
  }
}

MobileContainer.propTypes = {
  children: PropTypes.node,
}

const ResponsiveContainer = ({ activePage,loadPage,children }) => (
  <div>
    <DesktopContainer activePage={activePage} loadPage={loadPage}>{children}</DesktopContainer>
    <MobileContainer activePage={activePage} loadPage={loadPage}>{children}</MobileContainer>
  </div>
)

ResponsiveContainer.propTypes = {
  children: PropTypes.node,
}


class HomepageLayout extends React.Component{
  constructor(props) {
      super(props);
      this.state = {
          page:"",
          activePage:"Home"
      };
  }

  parentWindow = () => {
     return {loadPage:this.loadPage};
  }

  loadPage = (_page) => {
    console.log(_page);
    this.setState({page:_page,activePage:_page,loadingPage:true});
  };

  pageLoaded = (_page) => {
     this.setState({loadingPage:false});
  }

  render() {
    return(
    <ResponsiveContainer loadPage={this.loadPage} activePage={this.state.activePage} >    
      <PageManager page={this.state.page} pageLoaded={this.pageLoaded} parentWindow={this.parentWindow}  />  
      <Footer visible={!this.state.loadingPage} />
    </ResponsiveContainer>);
  }
}
export default HomepageLayout