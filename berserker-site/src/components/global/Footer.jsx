import React from 'react';
import {
  Container,
  Grid,
  Header,
  List,
  Segment,
  Icon
} from 'semantic-ui-react';

const Footer =(props) => {
    let {visible} = props;
    return (<Segment inverted vertical style={{ padding: '5em 0em', display:visible?"block":"none" }}>
    <Container>
      <Grid divided inverted stackable>
        <Grid.Row>
          <Grid.Column width={3}>
            <Header inverted as='h4' content='Servicios' />
            <List link inverted>
              <List.Item as='a'>Contables</List.Item>
              <List.Item as='a'>Laborales</List.Item>
              <List.Item as='a'>Servicioes Adicionales</List.Item>
              <List.Item as='a'>Apoyo al Emprendedor</List.Item>
            </List>
          </Grid.Column>
          <Grid.Column width={7}>
            <Header as='h4' inverted>
              Contacto
            </Header>
            <p>
                <Icon name='whatsapp' />&nbsp;+569-979-92453<br/>
                <Icon name='mail' />&nbsp;beserkerac@gmail.com
            </p>
          </Grid.Column>
        </Grid.Row>
      </Grid>
    </Container>
  </Segment>);
};

export default Footer;