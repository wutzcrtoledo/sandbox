import React from 'react';
import ReactDOM from 'react-dom';
import MainDrawer  from './components/mainDrawer';
import './css/index.css';
import './css/custom.css';

import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#00695c',
    },
    secondary: {
      main: '#e65100',
    },
  },
  typography: {
    useNextVariants: true,
  }
});

function Site() {
  return (
    <MuiThemeProvider theme={theme}>
       <MainDrawer />
    </MuiThemeProvider>
  );
}


function Square(props)  {
      return (
        <button 
            className="square"  onClick={props.onClick}
        >
          {props.value}
        </button>
      );    
}
  
class Board extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
          squares: Array(9).fill(null),
          xIsNext : true
        };
    }
    checkWinner(){
        const lines = [
            [0, 1, 2],
            [3, 4, 5],
            [6, 7, 8],
            [0, 3, 6],
            [1, 4, 7],
            [2, 5, 8],
            [0, 4, 8],
            [2, 4, 6],
          ];
        let squares = this.state.squares;
        for(let i in lines){
            const [a, b, c] = lines[i];
            if(squares[a] && squares[a] === squares[b] && squares[a] === squares[c])
               return "Winner ! :" + squares[a];
        }
        return null;
    }
    handleClick(i) {
        const squares = this.state.squares.slice();
        if(squares[i] || this.checkWinner())
          return false;
        squares[i] = this.state.xIsNext?'X':'O';
        this.setState({squares: squares,  xIsNext: !this.state.xIsNext});
    }    
    renderSquare(i) {
      return <Square value={this.state.squares[i]} 
                     onClick={() => this.handleClick(i)} />;
    }  
    render() {
      const status = this.checkWinner()||'Next player: ' + (this.state.xIsNext?'X':'O');
  
      return (
        <div>
          <div className="status">{status}</div>
          <div className="board-row">
            {this.renderSquare(0)}
            {this.renderSquare(1)}
            {this.renderSquare(2)}
          </div>
          <div className="board-row">
            {this.renderSquare(3)}
            {this.renderSquare(4)}
            {this.renderSquare(5)}
          </div>
          <div className="board-row">
            {this.renderSquare(6)}
            {this.renderSquare(7)}
            {this.renderSquare(8)}
          </div>
        </div>
      );
    }
  }
  
  class Game extends React.Component {
    render() {
      return (
        <div className="game">
          <div className="game-board">
            <Board />
          </div>
          <div className="game-info">
            <div>{/* status */}</div>
            <ol>{/* TODO */}</ol>
          </div>
        </div>
      );
    }
  }
  
  // ========================================
 
  ReactDOM.render(
    <Site />,
    document.getElementById('root')
  );
  
