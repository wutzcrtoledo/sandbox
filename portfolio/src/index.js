import React from 'react';
import ReactDOM from 'react-dom';
import App  from './components/App';
import './css/index.css';
import './css/custom.css';
import { MuiThemeProvider, createMuiTheme } from '@material-ui/core/styles';

const theme = createMuiTheme({
  palette: {
    primary: {
      main: '#00695c',
    },
    secondary: {
      main: '#e65100',
    },
  },
  typography: {
    useNextVariants: true,
  }
});

const alertDefaultOptions = {
  position: 'top right',
  timeout: 5000,
  offset: '30px',
  transition: 'scale'
};

function Site() {
  return (
    <MuiThemeProvider theme={theme}>
          <App />
    </MuiThemeProvider>
  );
}
  // ========================================
 
  ReactDOM.render(
    <Site />,
    document.getElementById('root')
  );
  
