import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Typography from '@material-ui/core/Typography';
import Card from '@material-ui/core/Card';
import CardActionArea from '@material-ui/core/CardActionArea';
import CardHeader from '@material-ui/core/CardHeader';
import Avatar from '@material-ui/core/Avatar';
import CardContent from '@material-ui/core/CardContent';
import CardActions from '@material-ui/core/CardActions';
import Lightbox from 'react-images';
import Gallery from 'react-photo-gallery';
import AppBar from '@material-ui/core/AppBar';
import Toolbar from '@material-ui/core/Toolbar';
import Grid from '@material-ui/core/Grid';
import IconButton from '@material-ui/core/IconButton';

//import projectList from '../projects.json';
let $ = require("jquery");

const styles = theme => ({
  root: {
    display: 'flex',
    flexWrap: 'wrap',
    justifyContent: 'space-around',
    overflow: 'hidden',
    backgroundColor: theme.palette.background.paper,
  },
  gallContainer: {
    maxHeight: 180,
    marginBottom: 40, 
    overflowX: "auto"
  },
  gridList: {
    //width: "100%",
    flexWrap: 'nowrap',
    transform: 'translateZ(0)',
  },
  card: {
    maxWidth: 500,
    margin:15 
  },
  media: {
    //height: 140,
  },
  portfolioItem: {
     width: 300
  },
  mainAvatar: {
    width: 80,
    height: 80
  },
  appBar: {
    top: 0,
    bottom: 'auto',
  },
  mainGrid:{
    paddingTop: 170
  },
  langContainer:{
    position:"absolute",
    top:"30px",
    right:"10%"
  }
});

function LangButton(props){
   const buttClasses ={
      flag: {
        width: props.opened?50:30,
        height: props.opened?50:30
      }
   };
   return (
     <IconButton aria-label={props.label} onClick={props.onClick}>
        <img src={props.flag} alt={props.label} title={props.label} style={buttClasses.flag}  />
     </IconButton>
    );
}

class ProjectCard extends React.Component {
  constructor(props){
    super(props);
    this.styles = {
        avatarImg:{
          width: 40,
          height: 40
        }
    };
    this.state = { currentImage: 0, 
                   projAvatar:props.projAvatar ,                   
                 };
    this.closeLightbox = this.closeLightbox.bind(this);
    this.openLightbox = this.openLightbox.bind(this);
    this.gotoNext = this.gotoNext.bind(this);
    this.gotoPrevious = this.gotoPrevious.bind(this);
    if(/\.png|\.jpg|\.ico/ig.test(this.state.projAvatar)){
      this.state.projAvatar =  <img src={this.state.projAvatar} style={this.styles.avatarImg}  />;
    }
  }
  openLightbox(event, obj) {
    this.setState({
      currentImage: obj.index,
      lightboxIsOpen: true,
    });
  }
  closeLightbox() {
    this.setState({
      currentImage: 0,
      lightboxIsOpen: false,
    });
  }
  gotoPrevious() {
    this.setState({
      currentImage: this.state.currentImage - 1,
    });
  }
  gotoNext() {
    this.setState({
      currentImage: this.state.currentImage + 1,
    });
  }

  render() {
    const { classes } = this.props; 

    return (
    <div>
    <Card className={classes.card}>
      <CardActionArea>
        <CardHeader
          avatar={
            <Avatar aria-label="Recipe" className={classes.avatar}>
              {this.state.projAvatar}
            </Avatar>
          }          
          title={this.props.projName}
          subheader={this.props.projSummary}
        />
        <CardContent>
          <Typography component="p">
              {this.props.projDescription}
          </Typography>
        </CardContent>
        <CardContent className={classes.gallContainer}>
            <Gallery photos={this.props.images} margin={5} direction={"row"} onClick={this.openLightbox}/>         
        </CardContent>
      </CardActionArea> 
      <CardActions></CardActions>     
    </Card>
    <Lightbox images={this.props.images}
                onClose={this.closeLightbox}
                onClickPrev={this.gotoPrevious}
                onClickNext={this.gotoNext}
                currentImage={this.state.currentImage}
                isOpen={this.state.lightboxIsOpen}
            />
    </div>
  )}
}

class App extends React.Component {

  constructor(props) {
    super(props);
    this.state = {
      projectList: [],
      locale:{},
      sessLang:{en:{id:"en",label:"English",flag:"/img/en.jpg",opened:false},
                es:{id:"es",label:"Español",flag:"/img/es.jpg",opened:false}}
    };
   // let projL = require("/json/projetcs.json");
    
  }   

  componentDidMount() {
    window.addEventListener('load', this.handleLoad);
    let appComp = this;
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "/json/projects.json",
      success: (output, status, xhr) => {
        appComp.setState({
          projectList: output          
        });
      },
      error: (xhr, txtStat, errThrown) => {
          console.log(errThrown);
      }
    });   
    this.loadLocaleAttributes(); 
  }


  loadLocaleAttributes(lang) {
    lang = lang || "en";
    let appComp = this;
    $.ajax({
      type: "GET",
      dataType: 'json',
      url: "/locale/"+lang+".json",
      success: (output, status, xhr) => {

        let currState = appComp.state;
        currState.locale = output;
        for(var ln in currState.sessLang){
          currState.sessLang[ln].opened = false;
        }
        currState.sessLang[lang].opened=true;
        appComp.setState(currState);
      },
      error: (xhr, txtStat, errThrown) => {
          console.log(errThrown);
      }
    });    
  }

  handleLoad() {
   // console.log($);
    console.log($(window).height());
    //$("main").height($(window).height()-65);
    //this.checkSession();
  } 

  handleChangeLang(lang){
   // let lang = this.state.sessLang.id === "en"?"es":"en";
    this.loadLocaleAttributes(lang); 
  }

  render() {
    const { classes, theme } = this.props;   

    return (
      <div className={classes.root}>
        <AppBar position="fixed" color="primary" className={classes.appBar}>
          <Toolbar>
              <CardHeader
                  avatar={
                    <Avatar aria-label="Recipe" className={classes.mainAvatar}>
                      <img src="/img/avatar.jpg" alt="" className={classes.mainAvatar} />
                    </Avatar>
                  }          
                  title={<Typography variant="h6" style={{color:"#dddddd"}}>
                            {this.state.locale.title}
                        </Typography>}
                  subheader={<Typography variant="subtitle1" align="center">
                                  <a style={{color:"#dddddd"}} href={"mailto:crtoledo@bluezinc.cl"}>crtoledo@bluezinc.cl</a>
                            </Typography>}
                />
                <div className={classes.langContainer}>
                    <LangButton 
                        label={this.state.sessLang.en.label} 
                        flag={this.state.sessLang.en.flag} 
                        opened={this.state.sessLang.en.opened} 
                        onClick={this.handleChangeLang.bind(this,"en")}
                    />
                    <LangButton 
                        label={this.state.sessLang.es.label} 
                        flag={this.state.sessLang.es.flag} 
                        opened={this.state.sessLang.es.opened}
                        onClick={this.handleChangeLang.bind(this,"es")}
                    />
                </div>
          </Toolbar>
        </AppBar>
        <Grid
            container
            spacing={8}
            className={classes.mainGrid}
            alignItems={"flex-start"}
            direction={"row"}
            justify={"space-evenly"}
          >
          {this.state.projectList.map((project, i) => 
            <ProjectCard  key={project.key} 
                          projName={project.projName} 
                          projAvatar={project.projAvatar}  
                          projSummary={project.projSummary}
                          projDescription={this.state.locale[project.key+".projDescription"]}  
                          images={project.images} 
                          classes={classes} 
            />
          )}
        </Grid>
      </div>
    );
  }
}

App.propTypes = {
  classes: PropTypes.object.isRequired,
  theme: PropTypes.object.isRequired,
};

export default withStyles(styles, { withTheme: true })(App);
