import React from 'react';
import { withStyles } from '@material-ui/core/styles';
import GenericTable from '../custComp/GenericTable';
import moment from 'moment-timezone';
import _ from 'underscore';
import Delegate from '../../lib/Delegate';

const styles = theme => ({
    mainContainer: {
        //paddingTop: 20,
        width: '100%',
    }
    
  });

function ReportTable(props){
    let headerRows = [{ id: 'event_id', numeric: true, disablePadding: true, label: 'Id de Evento' },
                      { id: 'date', numeric: false, disablePadding: false, label: 'Fecha' },
                      { id: 'room', numeric: false, disablePadding: false, label: 'Habitación' },
                      { id: 'time', numeric: false, disablePadding: false, label: 'Hora' }];
    let data = [{
                  "event_id": 797,
                  "when":"05-06-2018",
                  "sensor_name": "Habitacion 8",
                  "time": "12:12:13"
                },
                {
                    "event_id": 798,
                    "when":"06-06-2018",
                    "sensor_name": "Habitacion 8",
                    "time": "12:12:13"
                  },
                  {
                    "event_id": 799,
                    "when":"07-06-2018",
                    "sensor_name": "Habitacion 8",
                    "time": "12:12:13"
                  }];
    let title = "Any Date", subtitle = "Subtitle Test";    

    return(<GenericTable headerRows={headerRows} data={data} tabTitle={title} tabSubTitle={subtitle} />);
};


class DateRangeMod extends React.Component {
  
  constructor(props) {
    super(props);
    this.state = {      
      selectedDate: null,   
      cameras : {},
      camerasMeta:null
    };
    this.deleg = new Delegate();
  }



  render() {
    const { classes, theme } = this.props; 
    return (
       <div>   
            <ReportTable />   
       </div>
    );
  }
}

export default withStyles(styles, { withTheme: true })(DateRangeMod);