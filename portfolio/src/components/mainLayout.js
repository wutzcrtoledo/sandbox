import React from 'react';
import PropTypes from 'prop-types';
import { withStyles } from '@material-ui/core/styles';
import Grid from '@material-ui/core/Grid';
import Paper from '@material-ui/core/Paper';
import HeaderComp from './headerComp';
import LeftNav from './leftNav';
import BottomNav from './bottomNav';
// export for others scripts to use
//window.$ = $;
const $ = require('jquery');

const styles = theme => ({
  root: {
    flexGrow: 1,
  },
  paper: {
    padding: theme.spacing.unit * 2,
    textAlign: 'center',
    color: theme.palette.text.secondary,
  }
});


class FullWidthGrid extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      isMobile: window.innerWidth <= 760, 
      openedLeftMenu: !(window.innerWidth <= 760),
      crdLeftMenu:{xs:3,md:3},
      crdMain:{xs:12,md:9},
      defaultLNClass: window.innerWidth <= 760?'leftNavMobileView':'leftNavDesktop'   
    };
    if(this.state.isMobile){
      this.state.crdLeftMenu.xs = false;
    }
  }
  
  render() {
    let mainCompObj = this;
    const { classes } = this.props;
    let leftMenuFnc = {
       openCloseLeftMenu : (ev) => {
           console.log("Worked Call to Parent");
           if(!this.state.isMobile){
              if(!mainCompObj.state.crdLeftMenu.xs){
                $(".leftNavDesktop").show();
                mainCompObj.setState({crdLeftMenu:{xs:3,md:3},
                                      crdMain:{xs:9,md:9}});
              }
              else{
              //  mainCompObj.setClasses({leftNavDesktop:{display:"none"}});
                $(".leftNavDesktop").hide();
                mainCompObj.setState({crdLeftMenu:{xs:false,md:false},
                  crdMain:{xs:12,md:12}});
              }
           }
           else{
            if(!mainCompObj.state.crdLeftMenu.xs){
              $(".leftNavMobileView").show();
              mainCompObj.setState({crdLeftMenu:{xs:6,md:true}});
             }
             else{
              $(".leftNavMobileView").hide();
              mainCompObj.setState({crdLeftMenu:{xs:false,md:false}});
             }
           }
       }
    };

      return (
        <div className={classes.root}>
          <Grid container spacing={8} justify="flex-start" alignItems="stretch" direction="row">  
            <Grid item className={this.state.defaultLNClass} xs={this.state.crdLeftMenu.xs} md={this.state.crdLeftMenu.md}>
              <Paper elevation="5">
                <LeftNav isMobile={this.state.isMobile} />   
              </Paper>   
            </Grid>
            <Grid item xs={this.state.crdMain.xs} md={this.state.crdMain.md}>
              <HeaderComp callbackFnc={leftMenuFnc} />
              <div>Main Container</div>
            </Grid>              
          </Grid>
        </div>
      );
  }
}

FullWidthGrid.propTypes = {
  classes: PropTypes.object.isRequired,
};

export default withStyles(styles)(FullWidthGrid);
