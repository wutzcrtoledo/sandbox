import React from 'react';

import MainMod  from './mods/MainMod';
import DateRangeMod from './mods/DateRangeMod';


class ModsController extends React.Component {
    constructor(props) {
      super(props);
      this.state = {
        open: props.open,
      //  selectedDate: props.selectedDate,
      };

    }

    setModuleState(modName, props){
        console.log(modName, props);
    }

  render() {
        if(this.props.mod === "mainMod"){
            return(<MainMod parentPipe={this.props.parentPipe} />);
        }
        else if(this.props.mod === "dateRange"){
            return(<DateRangeMod parentPipe={this.props.parentPipe} />);
        }
       return(null);
  }
}

export default (ModsController);