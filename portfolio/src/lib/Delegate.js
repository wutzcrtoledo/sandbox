import RestAdmin from './RestAdmin';

class Delegate {
    constructor(props) {
       this._logged = false;
       this.restInst = new RestAdmin(); 
    }

    set logged(_val){
        this._logged =_val;
    }

    get logged(){
        return this._logged;
    }

    login(_user, callback){
        console.log("init login");
        let delObj = this;
        delObj.restInst.callService("/login",{body:_user,method:"POST"}, _res => {
            delObj.logged = _res.logged;
            callback(_res);
        });
    }
    changePassword(_currPass, _newPass, callback){
        this.restInst.callService("/changePassword",{body:{currentPassword:_currPass, newPassword:_newPass},method:"POST"}, callback);
    }
    getEvents(_date, callback){
        this.restInst.callService("/getEvents/"+_date, callback);
    }
    getSensors(callback){
        this.restInst.callService("/getSensorsForUser/", callback);
    }
    
    logOut(callback){
        this.restInst.callService("/logout", callback);
        this.logged = false;
    }
  }
  export default (Delegate);