It's a helper for Sequelize Library to generate a file DB.js which contains 
the tables on you environment. Right now is working for MySQL Databases but can 
be extended.<br/>
<br/>
How it works. <br/>
<br/>
.- Download this repository<br/>
.- Set in config/default.json file the connection parameters of your database<br/>
.- run $ node createSynTablesObjMySQL<br/>
.- Once finished, the out directory. You should see the files MySQLDBClass.js <br/>
and DB.js. The second file should contain the list of tables on  your database 
configured and ready to be use using Sequelize Library. Copy those files and use 
them in you project as you like.


