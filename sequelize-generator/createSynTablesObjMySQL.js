const Sequelize = require('sequelize');
const fs = require("fs");
const _ = require("underscore");
var tables = {};

String.prototype.capitalize = function() {
  return this.charAt(0).toUpperCase() + this.slice(1);
};

var conf =  JSON.parse(fs.readFileSync("./config/default.json"));
var dbAccess = conf.mysql;
//var port = dbAccess.port || 3306;
const sequelize = new Sequelize(dbAccess.database, dbAccess.user, dbAccess.password, {
  host: dbAccess.host,
  port: (dbAccess.port || 3306),
  dialect: 'mysql',
  pool: {
    max: 5,
    min: 0,
    acquire: 30000,
    idle: 10000
  }, 
  operatorsAliases: false
});

var query = "SELECT TABLE_NAME FROM `INFORMATION_SCHEMA`.`TABLES` WHERE table_schema = '"+dbAccess.database+"'";
//var query = "select * from bz_user";
sequelize.query(query, { raw: true }).then((result) => {
  //console.log(result[0]);
  result = JSON.parse(JSON.stringify(result));
  result = _.uniq(result[0]);
 // console.log(result);
  var tableNames = _.map(result,"TABLE_NAME"); 
 // console.log(tableNames);
  var ping = 0, pong=0;
  _.each(tableNames, (_tableName)=>{
    if(!conf.tables2Process || conf.tables2Process.length === 0 || conf.tables2Process.indexOf(_tableName) !== -1){
        ping++;
        processTable(_tableName, ()=>{
            pong++;
            if(ping === pong){
                //console.log(JSON.stringify(tables));
                createObjFileFromTable();
                process.exit();
            }
        });
     }
  });
  
  //callback();
});

const map = {prefix: conf.prefix,
            type: {
               "varchar":"Sequelize.STRING",
               "text":"Sequelize.STRING",
               "longtext":"Sequelize.STRING",
               "float":"Sequelize.FLOAT",
               "timestamp":"Sequelize.DATE",
               "datetime":"Sequelize.DATE",
               "time":"Sequelize.TIME",
               "date":"Sequelize.DATE",
               "int":"Sequelize.INTEGER",
               "bigint":"Sequelize.BIGINT",
               "blob": "Sequelize.BLOB"
            }};
            
const processTable = (_tableName, callback) => {
  var objTabName = _tableName.replace(map.prefix,"");
  objTabName = objTabName.capitalize();
  objTabName = objTabName.replace(/\_[a-zA-Z0-9]+/ig, function(match){
     return match.replace("_","").capitalize();
  });
  objTabName = "tb"+objTabName;
  tables[_tableName] = {objName: objTabName, attributes:{}};
 // console.log(tables);
  var sql = "select * from information_schema.columns where table_schema = '"+dbAccess.database+"' and table_name = '"+_tableName+"' order by table_name,ordinal_position";
  sequelize.query(sql, { raw: true }).then((result) => {
    //console.log(result[0]);
    result = JSON.parse(JSON.stringify(result));
    result = _.uniq(result[0]);
    for(var i in result){
      var objColName = result[i].COLUMN_NAME.replace(/\_[a-zA-Z0-9]+/ig, function(match){
        return match.replace("_","").capitalize();
      });
      var col = {field: result[i].COLUMN_NAME, type: map.type[result[i].DATA_TYPE]||Sequelize.STRING};
      if(result[i].COLUMN_KEY && result[i].COLUMN_KEY === "PRI")
          col.primaryKey = true;
      if(result[i].IS_NULLABLE && result[i].IS_NULLABLE === "YES")
          col.allowNull = true;    
      if(result[i].EXTRA && result[i].EXTRA === "auto_increment")
          col.autoIncrement = true;
      tables[result[i].TABLE_NAME].attributes[objColName] = col;
    } 
    // console.log(result);   
     callback();
  });
};


const createObjFileFromTable = () => {
 
  var totalTxt = "";
  totalTxt += "var dbCl = require(\"./MySQLDBClass\");\n";
  totalTxt += "const Sequelize = require('sequelize'); \n\n";

  for(var tableName in tables){
    var node = tables[tableName];
    totalTxt += "const "+node.objName+" = dbCl.sequelize.define('"+tableName+"', {\n";
    var atts = node.attributes;
    var isFirst = true;
    for(var attObjId in atts){
      if(!isFirst)
        totalTxt +=",\n";
      isFirst = false;
      totalTxt += attObjId+": {field: '"+atts[attObjId].field+"', type: "+atts[attObjId].type;
      if(atts[attObjId].primaryKey)
        totalTxt +=", primaryKey: true";
      if(atts[attObjId].autoIncrement)
        totalTxt +=", autoIncrement: true";
      if(atts[attObjId].allowNull)
        totalTxt +=", allowNull: true";      
      totalTxt += "}";
    }
    totalTxt += "},\n {freezeTableName: true, timestamps:false});\n\n";
  }

  totalTxt += "module.exports = {\n";
  for(var tableName in tables){
    var node = tables[tableName]; 
    totalTxt += " "+node.objName+": "+node.objName+",\n";
  }
  totalTxt += "  query : dbCl.getJSONFromQuery";
  totalTxt += "};";

  console.log(totalTxt);
  var filePath = "out/DB.js";
 
  fs.createWriteStream(filePath);
  fs.appendFileSync(filePath, totalTxt, "utf8");
};